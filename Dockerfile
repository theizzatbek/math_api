# workspace (GOPATH) configured at /go
FROM golang:1.16.5-alpine as build

# Copy the local package files to the container's workspace.
RUN mkdir -p $GOPATH/src/gitlab.com/theizzatbek/math_api
WORKDIR $GOPATH/src/gitlab.com/theizzatbek/math_api

COPY . ./
RUN go mod vendor
# installing depends and build
# installing depends and build
RUN go build -o /out/math_api cmd/main.go

FROM alpine
COPY --from=build /out/math_api /
ENTRYPOINT ["/math_api"]