CURRENT_DIR=$(shell pwd)
APP=$(shell basename ${CURRENT_DIR})

APP_CMD_DIR=${CURRENT_DIR}/cmd

IMG_NAME=${APP}

make swag-gen:
	swag init -g cmd/main.go -o api/docs

build:
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${APP_CMD_DIR}/main.go

build-image:
	docker build -t ${IMG_NAME} .

run-image:
	 docker run -d -p 8080:8080 --name ${APP} ${IMG_NAME}

stop-container:
	docker rm --force ${APP}