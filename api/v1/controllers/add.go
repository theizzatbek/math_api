package controllers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/theizzatbek/math_api/api/v1/helpers"
	"gitlab.com/theizzatbek/math_api/api/v1/models"
	"net/http"
)

// Add - add two numbers
// @Router /v1/add [get]
// @Security ApiKeyAuth
// @Summary Get two numbers sum
// @Tags [v1] math
// @Accept json
// @Produce json
// @Param a query string false "number a"
// @Param b query string false "number b"
// @Success 200 {object} models.Response
// @Failure 400 {object} models.Response
func Add(c echo.Context) error {

	a, b, err := helpers.ParseTwoNumbers(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &models.Response{
			ErrCode: err.Error(),
		})
	}

	return c.JSON(http.StatusOK, &models.Response{
		Success: true,
		Value:   a + b,
	})
}
