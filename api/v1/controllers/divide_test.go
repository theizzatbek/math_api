package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.com/theizzatbek/math_api/api/v1/models"
	"gitlab.com/theizzatbek/math_api/common"
	"gitlab.com/theizzatbek/math_api/pkg/utils"
	"net/http"
	"testing"
)

func TestDiv(t *testing.T) {
	e := echo.New()
	tests := common.TestsForInvalidNumber
	tests = append(tests, models.Test{
		Name:    "division-by-zero",
		WantErr: true,
		Test: models.Query{
			A: "1",
			B: "0",
		},
		Actual: models.Response{
			Success: false,
			ErrCode: common.ErrDivisionByZero.Error(),
		},
		HttpStatus: http.StatusBadRequest,
	})

	tests = append(tests, models.Test{
		Name:    "ok",
		WantErr: false,
		Test: models.Query{
			A: "1",
			B: "2",
		},
		Actual: models.Response{
			Success: true,
			Value:   0.5,
		},
		HttpStatus: http.StatusOK,
	})
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {

			c, rec := utils.GetRequest(e, tt.Test.A, tt.Test.B)
			err := Div(c)
			if !tt.WantErr {
				assert.NoError(t, err)
			}
			body, _ := json.Marshal(tt.Actual)
			assert.Equal(t, tt.HttpStatus, rec.Code)
			assert.Equal(t, fmt.Sprintf("%s\n", body), rec.Body.String())
		})
	}
}
