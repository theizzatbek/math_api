package helpers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/theizzatbek/math_api/common"
	"strconv"
)

// ParseTwoNumbers - parse two numbers on query
func ParseTwoNumbers(c echo.Context) (float64, float64, error) {
	a, err := strconv.ParseFloat(c.QueryParam("a"), 64)
	if err != nil {
		return 0, 0, common.ErrInvalidSyntaxA
	}
	b, err := strconv.ParseFloat(c.QueryParam("b"), 64)
	if err != nil {
		return 0, 0, common.ErrInvalidSyntaxB
	}

	return a, b, nil
}
