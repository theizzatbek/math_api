package models

// Response - response return value struct
type Response struct {
	Success bool
	ErrCode string
	Value   float64
}
