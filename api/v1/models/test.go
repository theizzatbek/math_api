package models

type Query struct {
	A, B string
}

type Test struct {
	Name       string
	WantErr    bool
	Test       Query
	Actual     Response
	HttpStatus int
}
