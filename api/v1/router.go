package v1

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/theizzatbek/math_api/api/v1/controllers"
)

func PublicRoutesV1(e *echo.Echo) {
	group := e.Group("/api/v1")
	group.GET("/add", controllers.Add)
	group.GET("/div", controllers.Div)
	group.GET("/mul", controllers.Mul)
	group.GET("/sub", controllers.Sub)
}
