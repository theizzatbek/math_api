package ci

import (
	"gitlab.com/theizzatbek/math_api/config"
	"sync"
)

// Container is a global struct for application
type Container struct {
	Config *config.Config
	// Here you can add a logger and other necessary components for app
}

var instance *Container

var once sync.Once

func GetInstance() *Container {
	once.Do(func() {
		instance = &Container{}
	})

	return instance
}
