package main

import (
	_ "github.com/joho/godotenv/autoload" // load .env file automatically
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger"
	_ "gitlab.com/theizzatbek/math_api/api/docs" //swagger reg
	apiV1 "gitlab.com/theizzatbek/math_api/api/v1"
	"gitlab.com/theizzatbek/math_api/ci"
	"gitlab.com/theizzatbek/math_api/config"
	"log"
)

// @title Test task
// @version 1.0
// @description This is a sample server for test.

// @BasePath /api
// @query.collection.format multi

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {

	c := ci.GetInstance()
	c.Config = config.Get()

	e := echo.New()
	e.Use(middleware.Recover())

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	apiV1.PublicRoutesV1(e)

	if err := e.Start(c.Config.Port); err != nil {
		log.Fatal("error while running echo server", err)
	}
}
