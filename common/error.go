package common

import "errors"

var (
	ErrDivisionByZero = errors.New("division by zero")
	ErrInvalidSyntaxA = errors.New("a - invalid syntax")
	ErrInvalidSyntaxB = errors.New("b - invalid syntax")
)
