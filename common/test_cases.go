package common

import (
	"gitlab.com/theizzatbek/math_api/api/v1/models"
	"net/http"
)

var TestsForInvalidNumber = []models.Test{
	{
		Name:    "invalid-syntax-a",
		WantErr: true,
		Test: models.Query{
			A: "1a",
			B: "2",
		},
		Actual: models.Response{
			Success: false,
			ErrCode: ErrInvalidSyntaxA.Error(),
		},
		HttpStatus: http.StatusBadRequest,
	},
	{
		Name:    "invalid-syntax-b",
		WantErr: true,
		Test: models.Query{
			A: "1",
			B: "2b",
		},
		Actual: models.Response{
			Success: false,
			ErrCode: ErrInvalidSyntaxB.Error(),
		},
		HttpStatus: http.StatusBadRequest,
	},
}
