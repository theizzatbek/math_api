package config

import (
	"github.com/spf13/cast"
	"os"
	"sync"
)

type Config struct {
	Port string
}

func load() *Config {
	return &Config{
		Port: cast.ToString(getOrReturnDefault("PORT", ":8080")),
	}
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}

var (
	instance *Config
	once     sync.Once
)

func Get() *Config {
	once.Do(func() {
		instance = load()
	})

	return instance
}
