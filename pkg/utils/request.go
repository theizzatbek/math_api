package utils

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"net/http/httptest"
	"net/url"
)

func GetRequest(e *echo.Echo, a, b string) (echo.Context, *httptest.ResponseRecorder) {
	q := make(url.Values)
	q.Set("a", a)
	q.Set("b", b)

	req := httptest.NewRequest(http.MethodGet, "/api/v1/div?"+q.Encode(), nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	return c, rec
}
